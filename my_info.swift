let name = "Амир"
let surname = "Галимуллин"
let age = 21
let height = 194.1
let isMale = true
print("Имя:", name)
print("Фамилия:", surname)
print("Возраст:", age)
print("Рост:", height, "см")
print("Пол:", isMale ? "Мужской" : "Женский")